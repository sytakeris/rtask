package com.task

import com.task.domain.AccountRepository
import com.task.domain.AccountService
import com.task.domain.TransactionRepository
import com.task.web.AccountController
import com.task.web.Router
import com.task.web.TransactionController
import io.javalin.Javalin

fun main() {
    val accountRepository = AccountRepository()
    val transactionRepository = TransactionRepository()

    val accountService = AccountService(accountRepository, transactionRepository)

    val accountController = AccountController(accountRepository)
    val transactionController = TransactionController(accountService, transactionRepository)

    Javalin.create().also {
        val router = Router(accountController, transactionController)
        router.register(it)
    }.start(7000)
}
