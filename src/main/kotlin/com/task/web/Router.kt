package com.task.web

import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.*

class Router(
    private val accountController: AccountController,
    private val transactionController: TransactionController
) {
    fun register(app: Javalin) {
        app.routes {
            path("v1") {

                path("accounts") {
                    get(accountController::getAllAccounts)
                    get("/:id", accountController::getAccountById)
                    post(accountController::createAccount)
                }

                path("transactions") {
                    get(transactionController::getAllTransactions)
                    get("/:id", transactionController::getTransactionById)
                    post(transactionController::createTransaction)
                }
            }
        }
    }
}