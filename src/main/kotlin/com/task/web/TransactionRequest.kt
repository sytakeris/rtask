package com.task.web

import java.math.BigDecimal

data class TransactionRequest(
    val fromAccount: String?,
    val toAccount: String?,
    val amount: BigDecimal?
)