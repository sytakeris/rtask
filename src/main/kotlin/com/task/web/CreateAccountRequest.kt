package com.task.web

data class CreateAccountRequest(val firstName: String?, val lastName: String?)