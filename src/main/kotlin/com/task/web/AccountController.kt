package com.task.web

import com.task.domain.*
import io.javalin.http.Context

class AccountController(private val accountRepository: AccountRepository) {
    fun getAllAccounts(ctx: Context) {
        accountRepository.getAll().also { accounts ->
            ctx.json(accounts)
        }
    }

    fun createAccount(ctx: Context) {
        ctx.bodyValidator<CreateAccountRequest>()
            .check({ !it.firstName.isNullOrBlank() }, "firstName is required")
            .check({ !it.lastName.isNullOrBlank() }, "lastName is required")
            .get().also { (firstName, lastName) ->
                Account(firstName = firstName!!, lastName = lastName!!).apply {
                    addEntry(Entry.credit("-", 100.toBigDecimal()))
                }.also {
                    accountRepository.save(it)
                    ctx.json(it)
                }
            }
    }

    fun getAccountById(ctx: Context) {
        ctx.pathParam<String>("id").get().also { id ->
            val account = accountRepository.findById(id)
            if (account != null) {
                ctx.json(account)
            } else {
                ctx.status(404)
            }
        }
    }
}