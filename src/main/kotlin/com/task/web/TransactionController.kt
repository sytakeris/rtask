package com.task.web

import com.task.domain.*
import io.javalin.http.BadRequestResponse
import io.javalin.http.Context
import io.javalin.http.NotFoundResponse
import java.math.BigDecimal

class TransactionController(
    private val accountService: AccountService,
    private val transactionRepository: TransactionRepository
) {
    fun getAllTransactions(ctx: Context) {
        transactionRepository.getAll().also { transactions ->
            ctx.json(transactions)
        }
    }

    fun getTransactionById(ctx: Context) {
        ctx.pathParam<String>("id").get().also { id ->
            val transaction = transactionRepository.findById(id)
            if (transaction != null) {
                ctx.json(transaction)
            } else {
                ctx.status(404)
            }
        }
    }

    fun createTransaction(ctx: Context) {
        ctx.bodyValidator<TransactionRequest>()
            .check({ !it.fromAccount.isNullOrBlank() }, "fromAccount is required")
            .check({ !it.toAccount.isNullOrBlank() }, "toAccount is required")
            .check({ it.amount != null }, "amount is required")
            .check({ it.amount!! > BigDecimal.ZERO }, "amount needs to be greater than zero")
            .get().also { (fromAccount, toAccount, amount) ->
                accountService.transferMoney(fromAccount!!, toAccount!!, amount!!).also {
                    ctx.json(it)
                }
            }

    }
}