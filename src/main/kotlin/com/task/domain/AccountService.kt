package com.task.domain

import io.javalin.http.BadRequestResponse
import io.javalin.http.NotFoundResponse
import java.math.BigDecimal

class AccountService(
    private val accountRepository: AccountRepository,
    private val transactionRepository: TransactionRepository
) {
    fun transferMoney(fromAccountId: String, toAccountId: String, amount: BigDecimal): Transaction {
        val from = accountRepository.findById(fromAccountId) ?: throw NotFoundResponse("From account not found")
        val to = accountRepository.findById(toAccountId) ?: throw NotFoundResponse("To account not found")

        synchronized(this) {
            if (from.balance < amount) {
                throw BadRequestResponse("Not enough balance in account ${from.id}")
            }

            val transaction = Transaction(from.id, to.id, amount)

            from.addEntry(Entry.debit(transaction.id, amount))
            to.addEntry(Entry.credit(transaction.id, amount))
            transactionRepository.save(transaction)
            return transaction
        }
    }
}