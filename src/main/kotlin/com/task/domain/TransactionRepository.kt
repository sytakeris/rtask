package com.task.domain

import java.util.concurrent.ConcurrentHashMap

class TransactionRepository(private val dataStore:  MutableMap<String, Transaction> = ConcurrentHashMap()) {
    fun getAll(): Collection<Transaction> = dataStore.values
    fun save(transaction: Transaction) = dataStore.put(transaction.id, transaction)
    fun findById(id: String): Transaction? = dataStore[id]
}