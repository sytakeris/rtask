package com.task.domain

import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*

class Entry private constructor(
    val type: Type,
    val amount: BigDecimal,
    val transactionId: String
) {
    val id: String = UUID.randomUUID().toString()

    enum class Type { DEBIT, CREDIT }

    companion object {
        fun debit(transactionId: String, amount: BigDecimal) =
            Entry(transactionId = transactionId, type = Type.DEBIT, amount = amount.setScale(2, RoundingMode.HALF_UP))

        fun credit(transactionId: String, amount: BigDecimal) =
            Entry(transactionId = transactionId, type = Type.CREDIT, amount = amount.setScale(2, RoundingMode.HALF_UP))
    }

}