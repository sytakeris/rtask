package com.task.domain

import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*

class Account(
    val id: String = UUID.randomUUID().toString(),
    val firstName: String,
    val lastName: String
) {
    private val _entries: MutableList<Entry> = mutableListOf()
    val entries: List<Entry> = _entries
    fun addEntry(entry: Entry) = _entries.add(entry)


    val balance: BigDecimal
        get() = entries.fold(BigDecimal.ZERO) { acc, transaction ->
            when (transaction.type) {
                Entry.Type.DEBIT -> acc - transaction.amount
                Entry.Type.CREDIT -> acc + transaction.amount
            }
        }.setScale(2, RoundingMode.HALF_UP)
}