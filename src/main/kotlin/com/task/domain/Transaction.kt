package com.task.domain

import java.math.BigDecimal
import java.time.Instant
import java.util.*

class Transaction(val fromAccountId: String, val toAccountId: String, val amount: BigDecimal) {
    val id = UUID.randomUUID().toString()
    val timestamp = Instant.now().toString()
}