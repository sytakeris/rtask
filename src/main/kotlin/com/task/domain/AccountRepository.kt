package com.task.domain

import java.util.concurrent.ConcurrentHashMap

class AccountRepository(private val dataStore:  MutableMap<String, Account> = ConcurrentHashMap()) {
    fun getAll(): Collection<Account> = dataStore.values
    fun save(account: Account) = dataStore.put(account.id, account)
    fun findById(id: String): Account? = dataStore[id]
}