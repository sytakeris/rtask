package com.task

import com.task.domain.*
import com.task.web.AccountController
import com.task.web.Router
import com.task.web.TransactionController
import com.task.web.TransactionRequest
import io.javalin.Javalin
import kong.unirest.Unirest
import java.math.BigDecimal
import java.util.concurrent.Callable
import java.util.concurrent.CountDownLatch
import java.util.concurrent.Executors
import java.util.concurrent.Future
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicInteger

class AppTestConfig(val app: Javalin, val accountDataStore: MutableMap<String, Account>, val transactionDataStore: MutableMap<String, Transaction>)

fun setupApp(): AppTestConfig {
    val accountDataStore = mutableMapOf<String, Account>()
    val transactionDataStore = mutableMapOf<String, Transaction>()

    val accountRepository = AccountRepository(accountDataStore)
    val transactionRepository = TransactionRepository(transactionDataStore)


    val accountService = AccountService(accountRepository, transactionRepository)

    val accountController = AccountController(accountRepository)
    val transactionController = TransactionController(accountService, transactionRepository)

    val app = Javalin.create().also {
        val router = Router(accountController, transactionController)
        router.register(it)
    }

    return AppTestConfig(app, accountDataStore, transactionDataStore)
}

fun concurrentTest(threads: Int = 10, test: () -> Int) {
    val latch = CountDownLatch(1)
    val running = AtomicBoolean()
    val overlaps = AtomicInteger()
    val service = Executors.newFixedThreadPool(threads)
    val futures = mutableListOf<Future<Int>>().apply {

        for (i in 1..threads) {
            add(service.submit(Callable<Int> {
                latch.await()
                if (running.get()) {
                    overlaps.incrementAndGet()
                }
                running.set(true)
                val result = test()
                running.set(false)
                result
            }))
        }
    }
    latch.countDown()
    futures.forEach {it.get()}
    println(overlaps.get())
}