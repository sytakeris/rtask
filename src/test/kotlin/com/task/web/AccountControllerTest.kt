package com.task.web

import com.task.domain.*
import com.task.setupApp
import kong.unirest.GenericType
import kong.unirest.Unirest
import org.assertj.core.api.Assertions.*
import org.junit.jupiter.api.*


internal class AccountControllerTest {
    companion object {
        val testConfig = setupApp()
        val app = testConfig.app
        val accountDataStore = testConfig.accountDataStore

        @BeforeAll
        @JvmStatic
        fun setup() {
            app.start(7000)
        }

        @AfterAll
        @JvmStatic
        fun tearDown() {
            app.stop()
        }
    }

    @AfterEach
    fun afterEach() {
        accountDataStore.clear()
    }

    @Test
    fun `should fetch all accounts`() {
        val account = Account(firstName = "testName", lastName = "testLastName")
        accountDataStore[account.id] = account

        val response = Unirest.get("http://localhost:7000/v1/accounts")
            .asObject(object : GenericType<List<Account>>() {})
            .body

        assertThat(response).hasSize(1)

        assertThat(response[0].id).isEqualTo(account.id)
        assertThat(response[0].firstName).isEqualTo(account.firstName)
        assertThat(response[0].lastName).isEqualTo(account.lastName)
        assertThat(response[0].balance).isEqualTo(account.balance)
        assertThat(response[0].entries).isEqualTo(account.entries)
    }

    @Test
    fun `should create new account`() {
        val response = Unirest.post("http://localhost:7000/v1/accounts")
            .body(CreateAccountRequest("John", "Doe"))
            .asObject(Account::class.java)

        assertThat(response.status).isEqualTo(200)
        val account = response.body

        assertThat(response.body.id).isEqualTo(account.id)
        assertThat(response.body.firstName).isEqualTo(account.firstName)
        assertThat(response.body.lastName).isEqualTo(account.lastName)
        assertThat(response.body.balance).isEqualTo(account.balance)
        assertThat(response.body.entries).isEqualTo(account.entries)
    }

    @Test
    fun `should get account by id`() {
        val account = Account(firstName = "testName", lastName = "testLastName")
        accountDataStore[account.id] = account

        val response = Unirest.get("http://localhost:7000/v1/accounts/${account.id}")
            .asObject(Account::class.java)
            .body

        assertThat(response.id).isEqualTo(account.id)
        assertThat(response.firstName).isEqualTo(account.firstName)
        assertThat(response.lastName).isEqualTo(account.lastName)
        assertThat(response.balance).isEqualTo(account.balance)
        assertThat(response.entries).isEqualTo(account.entries)
    }

    @Test
    fun `should return 404 response when account not found`() {
        val account = Account(firstName = "testName", lastName = "testLastName")
        accountDataStore[account.id] = account

        val response = Unirest.get("http://localhost:7000/v1/accounts/123")
            .asObject(Account::class.java)

        assertThat(response.status).isEqualTo(404)
    }

    @Test
    fun `should return bad request when firstName or lastName is null or blank`() {
        arrayListOf(
            CreateAccountRequest(null, "Doe"),
            CreateAccountRequest("", "Doe"),
            CreateAccountRequest(" ", "Doe"),
            CreateAccountRequest("John", ""),
            CreateAccountRequest("John", " "),
            CreateAccountRequest("John", null)
        ).forEach { request ->
            val response = Unirest.post("http://localhost:7000/v1/accounts")
                .body(request)
                .asEmpty()

            assertThat(response.status).isEqualTo(400)
        }
    }
}