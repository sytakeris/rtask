package com.task.web

import com.task.concurrentTest
import com.task.domain.Account
import com.task.domain.Entry
import com.task.domain.Transaction
import com.task.setupApp
import kong.unirest.GenericType
import kong.unirest.Unirest
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.concurrent.Callable
import java.util.concurrent.CountDownLatch
import java.util.concurrent.Executors
import java.util.concurrent.Future
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicInteger


internal class TransactionControllerTest {
    companion object {
        val testConfig = setupApp()
        val app = testConfig.app
        val accountDataStore = testConfig.accountDataStore
        val transactionDataStore = testConfig.transactionDataStore

        @BeforeAll
        @JvmStatic
        fun setup() {
            app.start(7000)
        }

        @AfterAll
        @JvmStatic
        fun tearDown() {
            app.stop()
        }
    }

    @AfterEach
    fun afterEach() {
        accountDataStore.clear()
        transactionDataStore.clear()
    }

    @Test
    fun `should fetch all transactions`() {
        val transaction = Transaction("1", "1", 100.toBigDecimal())
        transactionDataStore[transaction.id] = transaction

        val response = Unirest.get("http://localhost:7000/v1/transactions")
            .asObject(object : GenericType<List<Transaction>>() {})
            .body

        assertThat(response).hasSize(1)
        assertThat(response.first())
            .isEqualToComparingFieldByField(transaction)
    }

    @Test
    fun `should get transaction by id`() {
        val transaction = Transaction("1", "1", 100.toBigDecimal())
        transactionDataStore[transaction.id] = transaction

        val response = Unirest.get("http://localhost:7000/v1/transactions/${transaction.id}")
            .asObject(Transaction::class.java)
            .body

        assertThat(response)
            .isEqualToComparingFieldByField(transaction)
    }

    @Test
    fun `should return 404 response when transaction not found`() {
        val transaction = Transaction("1", "1", 100.toBigDecimal())
        transactionDataStore[transaction.id] = transaction

        val response = Unirest.get("http://localhost:7000/v1/accounts/123")
            .asObject(Transaction::class.java)

        assertThat(response.status).isEqualTo(404)
    }

    @Test
    fun `should return 400 bad request when from account is not given`() {
        val account = Account(firstName = "testName", lastName = "testLastName")
        accountDataStore[account.id] = account
        listOf(
            TransactionRequest("", account.id, BigDecimal.TEN),
            TransactionRequest("  ", account.id, BigDecimal.TEN),
            TransactionRequest(null, account.id, BigDecimal.TEN)
        ).forEach { request ->
            val response = Unirest.post("http://localhost:7000/v1/transactions")
                .body(request)
                .asEmpty()

            assertThat(response.status).isEqualTo(400)
        }
    }

    @Test
    fun `should return 400 bad request when to account is not given`() {
        val account = Account(firstName = "testName", lastName = "testLastName")
        accountDataStore[account.id] = account
        listOf(
            TransactionRequest(account.id, "", BigDecimal.TEN),
            TransactionRequest(account.id, "  ", BigDecimal.TEN),
            TransactionRequest(account.id, null, BigDecimal.TEN)
        ).forEach { request ->
            val response = Unirest.post("http://localhost:7000/v1/transactions")
                .body(request)
                .asEmpty()

            assertThat(response.status).isEqualTo(400)
        }
    }

    @Test
    fun `should return 400 bad request when amount is not given`() {
        val fromAccount = Account(firstName = "testName", lastName = "testLastName")
        val toAccount = Account(firstName = "testName", lastName = "testLastName")
        accountDataStore[fromAccount.id] = fromAccount
        accountDataStore[toAccount.id] = toAccount

        val response = Unirest.post("http://localhost:7000/v1/transactions")
            .body(TransactionRequest(fromAccount.id, toAccount.id, null))
            .asEmpty()

        assertThat(response.status).isEqualTo(400)
    }

    @Test
    fun `should return 400 bad request when amount is zero`() {
        val fromAccount = Account(firstName = "testName", lastName = "testLastName")
        val toAccount = Account(firstName = "testName", lastName = "testLastName")
        accountDataStore[fromAccount.id] = fromAccount
        accountDataStore[toAccount.id] = toAccount

        val response = Unirest.post("http://localhost:7000/v1/transactions")
            .body(TransactionRequest(fromAccount.id, toAccount.id, BigDecimal.ZERO))
            .asEmpty()

        assertThat(response.status).isEqualTo(400)
    }

    @Test
    fun `should return 404 not found when from account does not exist`() {
        val fromAccount = Account(firstName = "testName", lastName = "testLastName")
        val toAccount = Account(firstName = "testName", lastName = "testLastName")
        accountDataStore[toAccount.id] = toAccount

        val response = Unirest.post("http://localhost:7000/v1/transactions")
            .body(TransactionRequest(fromAccount.id, toAccount.id, BigDecimal.TEN))
            .asEmpty()

        assertThat(response.status).isEqualTo(404)
    }

    @Test
    fun `should return 404 not found when to account does not exist`() {
        val fromAccount = Account(firstName = "testName", lastName = "testLastName")
        val toAccount = Account(firstName = "testName", lastName = "testLastName")
        accountDataStore[fromAccount.id] = fromAccount

        val response = Unirest.post("http://localhost:7000/v1/transactions")
            .body(TransactionRequest(fromAccount.id, toAccount.id, BigDecimal.TEN))
            .asEmpty()

        assertThat(response.status).isEqualTo(404)
    }

    @Test
    fun `should return 400 bad request when from account does not have enough funds`() {
        val fromAccount = Account(
            firstName = "testName",
            lastName = "testLastName"
        ).apply {
            addEntry(Entry.credit("1", 1.toBigDecimal()))
        }

        val toAccount = Account(firstName = "testName", lastName = "testLastName")
        accountDataStore[fromAccount.id] = fromAccount
        accountDataStore[toAccount.id] = toAccount

        val response = Unirest.post("http://localhost:7000/v1/transactions")
            .body(TransactionRequest(fromAccount.id, toAccount.id, BigDecimal.TEN))
            .asEmpty()

        assertThat(response.status).isEqualTo(400)
    }

    @Test
    fun `should transfer money from one account to another`() {
        val fromAccount = Account(
            firstName = "testName",
            lastName = "testLastName"
        ).apply {
            addEntry(Entry.credit("1", 100.toBigDecimal()))
        }

        val toAccount = Account(firstName = "testName", lastName = "testLastName")
        accountDataStore[fromAccount.id] = fromAccount
        accountDataStore[toAccount.id] = toAccount

        val response = Unirest.post("http://localhost:7000/v1/transactions")
            .body(TransactionRequest(fromAccount.id, toAccount.id, BigDecimal.TEN))
            .asObject(Transaction::class.java)

        assertThat(response.status).isEqualTo(200)
        assertThat(response.body.amount).isEqualTo(BigDecimal.TEN)

        assertThat(fromAccount.balance).isEqualTo(90.toBigDecimal().setScale(2, RoundingMode.HALF_UP))
        assertThat(fromAccount.entries).hasSize(2)
        assertThat(fromAccount.entries.find { it.transactionId == response.body.id })
            .isNotNull()
            .isEqualToComparingOnlyGivenFields(
                Entry.debit(response.body.id, BigDecimal.TEN),
                "transactionId",
                "amount",
                "type"
            )

        assertThat(toAccount.balance).isEqualTo(BigDecimal.TEN.setScale(2, RoundingMode.HALF_UP))
        assertThat(toAccount.entries).hasSize(1)
        assertThat(toAccount.entries.first())
            .isEqualToComparingOnlyGivenFields(
                Entry.credit(response.body.id, BigDecimal.TEN),
                "transactionId",
                "amount",
                "type"
            )
    }

    @Test
    fun `should handle multiple concurrent transactions when account has enough balance`() {
        val fromAccount = Account(
            firstName = "testName",
            lastName = "testLastName"
        ).apply {
            addEntry(Entry.credit("1", 100.toBigDecimal()))
        }

        val toAccount = Account(firstName = "testName", lastName = "testLastName")
        accountDataStore[fromAccount.id] = fromAccount
        accountDataStore[toAccount.id] = toAccount

        concurrentTest(20) {
            val response = Unirest.post("http://localhost:7000/v1/transactions")
                .body(TransactionRequest(fromAccount.id, toAccount.id, BigDecimal.TEN))
                .asEmpty()
            response.status
        }

        assertThat(fromAccount.balance).isEqualTo(BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP))
        assertThat(toAccount.balance).isEqualTo(100.toBigDecimal().setScale(2, RoundingMode.HALF_UP))
    }

    @Test
    fun `should handle multiple concurrent transactions when account does not have balance for all transactions`() {
        val fromAccount = Account(
            firstName = "testName",
            lastName = "testLastName"
        ).apply {
            addEntry(Entry.credit("1", 75.toBigDecimal()))
        }

        val toAccount = Account(firstName = "testName", lastName = "testLastName")
        accountDataStore[fromAccount.id] = fromAccount
        accountDataStore[toAccount.id] = toAccount

        concurrentTest(20) {
            val response = Unirest.post("http://localhost:7000/v1/transactions")
                .body(TransactionRequest(fromAccount.id, toAccount.id, BigDecimal.TEN))
                .asEmpty()
            response.status
        }

        assertThat(fromAccount.balance).isEqualTo(5.toBigDecimal().setScale(2, RoundingMode.HALF_UP))
        assertThat(toAccount.balance).isEqualTo(70.toBigDecimal().setScale(2, RoundingMode.HALF_UP))
    }
}