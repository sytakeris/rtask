package com.task.domain

import org.assertj.core.api.Assertions.*
import org.junit.jupiter.api.Test
import java.math.BigDecimal
import java.math.RoundingMode

internal class AccountTest {

    @Test
    fun `should have balance zero when there no transaction`() {
        val account = Account(firstName = "first", lastName = "last")
        assertThat(account.balance).isEqualTo(BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP))
    }

    @Test
    fun `should have correct balance`() {
        val account = Account(firstName = "first", lastName = "last").apply {
            addEntry(Entry.credit("1", 100.toBigDecimal()))
            addEntry(Entry.credit("2", 150.toBigDecimal()))
            addEntry(Entry.credit("3", 10.50.toBigDecimal()))
            addEntry(Entry.debit("4", 30.55.toBigDecimal()))
        }

        assertThat(account.balance).isEqualTo(229.95.toBigDecimal())
    }
}