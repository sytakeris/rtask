# Backend task

## Running application
Run `shadowJar` gradle task. This will build uberJar in `build/libs`. When finished run the built jar.
Server will be running on `7000` port
* **You can use already prebuilt `app.jar`**
## Quick overview
There are three main entities
   * Account - It contains owner of the account and list of entries to keep the balance
   * Entry - This is account entry that show debited and credited sums by transaction
   * Transaction - Entity representing transaction. It show amount being transferred from one account to another

Resources for creating accounts and transactions are exposed via rest api
 
## Note
For simplicity sake I assumed that application does not care about currency

## Transferring money between accounts 

* Create two accounts. For convenience newly created accounts have 100 balance
```bash
curl --location --request POST 'localhost:7000/v1/accounts' \
--header 'Content-Type: application/json' \
--data-raw '{
	"firstName": "John",
	"lastName": "Doe"
}'
```

* Send money from one account to another
```bash
curl --location --request POST 'localhost:7000/v1/transactions' \
--header 'Content-Type: application/json' \
--data-raw '{
	"fromAccount": <created account id>,
	"toAccount": <created account id>,
	"amount": <amount>
}'
```